package innometrics.agents.listener.webhook.jira.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Document(collection = "jira" + "_issue_assign_event")
@ToString
public class IssueAssignEvent {

    @Id
    String user;

    String issueId;

    Date created;

    String assignee;



}
