package innometrics.agents.listener.webhook.jira.repository;

import innometrics.agents.listener.webhook.jira.model.LogWorkEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LogWorkEventRepository extends MongoRepository<LogWorkEvent,String> {
}
