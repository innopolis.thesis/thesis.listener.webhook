package innometrics.agents.listener.webhook.jira.repository;

import innometrics.agents.listener.webhook.jira.model.IssueUpdateEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueUpdateEventRepository extends MongoRepository<IssueUpdateEvent,String> {
}
