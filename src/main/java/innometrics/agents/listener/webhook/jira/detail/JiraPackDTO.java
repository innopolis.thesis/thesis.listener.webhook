package innometrics.agents.listener.webhook.jira.detail;

import innometrics.agents.listener.webhook.jira.model.*;
import lombok.Data;

import java.util.List;

@Data
public class JiraPackDTO {

    List<AddCommentEvent> addCommentEvents;

    List<IssueAssignEvent> issueAssignEvents;

    List<IssueCreatedEvent> issueCreatedEvents;

    List<IssueUpdateEvent> issueUpdateEvents;

    List<LogWorkEvent> logWorkEvents;

    public JiraPackDTO(List<AddCommentEvent> addCommentEvents, List<IssueAssignEvent> issueAssignEvents, List<IssueCreatedEvent> issueCreatedEvents, List<IssueUpdateEvent> issueUpdateEvents, List<LogWorkEvent> logWorkEvents) {
        this.addCommentEvents = addCommentEvents;
        this.issueAssignEvents = issueAssignEvents;
        this.issueCreatedEvents = issueCreatedEvents;
        this.issueUpdateEvents = issueUpdateEvents;
        this.logWorkEvents = logWorkEvents;
    }
}
