package innometrics.agents.listener.webhook.jira.repository;

import innometrics.agents.listener.webhook.jira.model.IssueAssignEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueAssignEventRepository extends MongoRepository<IssueAssignEvent,String> {
}
