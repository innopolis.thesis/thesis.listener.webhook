package innometrics.agents.listener.webhook.jira.model;

import lombok.*;
import lombok.experimental.FieldDefaults;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
@AllArgsConstructor
@Document(collection = "jira" + "_log_work_event")
@ToString
public class LogWorkEvent {

    @Id
    String user;

    String issueId;

    Long second;

}
