package innometrics.agents.listener.webhook.jira.repository;

import innometrics.agents.listener.webhook.jira.model.IssueCreatedEvent;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IssueCreatedEventRepository extends MongoRepository<IssueCreatedEvent,String> {
}
