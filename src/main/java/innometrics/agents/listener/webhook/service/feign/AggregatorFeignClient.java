package innometrics.agents.listener.webhook.service.feign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@FeignClient(
        name = "aggregator",
        url = "http://"+"${webhook.aggregator.ip}"+":"+"${webhook.aggregator.port}"
)
public interface AggregatorFeignClient {

    @PostMapping("metric")
    void saveMetrics(@RequestBody List metrics, @RequestParam(name = "type") String type);
}
