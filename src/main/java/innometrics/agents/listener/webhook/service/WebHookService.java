package innometrics.agents.listener.webhook.service;

import feign.Feign;
import innometrics.agents.listener.webhook.jira.detail.JiraPackDTO;
import innometrics.agents.listener.webhook.jira.model.*;
import innometrics.agents.listener.webhook.jira.repository.*;
import innometrics.agents.listener.webhook.service.feign.AggregatorFeignClient;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.cloud.openfeign.support.SpringMvcContract;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class WebHookService implements ApplicationRunner {


    @Autowired
    AddCommentEventRepository addCommentEventRepository;

    @Autowired
    IssueAssignEventRepository issueAssignEventRepository;

    @Autowired
    IssueCreatedEventRepository issueCreatedEventRepository;

    @Autowired
    IssueUpdateEventRepository issueUpdateEventRepository;

    @Autowired
    LogWorkEventRepository logWorkEventRepository;

    @Autowired
    AggregatorFeignClient aggregatorFeignClient;

    @Value("${webhook.period}")
    Long PERIOD;

//    @Value("${webhook.aggregator.ip}")
//    String AGGREGATOR_IP;
//
//    @Value("${webhook.aggregator.port}")
//    String AGGREGATOR_PORT;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        //jira
        boolean flag = true;
//        AggregatorFeignClient aggregatorFeignClient = Feign.builder()
//                .client(new OkHttpClient())
//                .encoder(new GsonEncoder())
//                .decoder(new GsonDecoder())
//                .contract(new SpringMvcContract())
////                .logger(new Slf4jLogger(BookClient.class))
////                .logLevel(Logger.Level.FULL)
//                .target(AggregatorFeignClient.class, String.format("http://%s:%s", AGGREGATOR_IP, AGGREGATOR_PORT));


        log.info("Sender started job");

        while (flag) {

            List<AddCommentEvent> addCommentEvents = addCommentEventRepository.findAll();
            List<IssueAssignEvent> issueAssignEvents = issueAssignEventRepository.findAll();
            List<IssueCreatedEvent> issueCreatedEvents = issueCreatedEventRepository.findAll();
            List<IssueUpdateEvent> issueUpdateEvents = issueUpdateEventRepository.findAll();
            List<LogWorkEvent> logWorkEvents = logWorkEventRepository.findAll();

            if (!addCommentEvents.isEmpty() || !issueAssignEvents.isEmpty() || !issueCreatedEvents.isEmpty() || !issueUpdateEvents.isEmpty() || !logWorkEvents.isEmpty()) {
//                JiraPackDTO jiraPack = new JiraPackDTO(addCommentEvents, issueAssignEvents, issueCreatedEvents, issueUpdateEvents, logWorkEvents);

                try {
                    aggregatorFeignClient.saveMetrics(addCommentEvents,"AddCommentEvent");
                    aggregatorFeignClient.saveMetrics(issueAssignEvents,"IssueAssignEvent");
                    aggregatorFeignClient.saveMetrics(issueCreatedEvents,"IssueCreatedEvent");
                    aggregatorFeignClient.saveMetrics(issueUpdateEvents,"IssueUpdateEvent");
                    aggregatorFeignClient.saveMetrics(logWorkEvents,"LogWorkEvent");

                    addCommentEventRepository.deleteAll(addCommentEvents);
                    issueAssignEventRepository.deleteAll(issueAssignEvents);
                    issueCreatedEventRepository.deleteAll(issueCreatedEvents);
                    issueUpdateEventRepository.deleteAll(issueUpdateEvents);
                    logWorkEventRepository.deleteAll(logWorkEvents);
                } catch (Exception e) {
                    log.error("Sended data was not accepted");
                    e.printStackTrace();
                    flag = false;
                }


            }


            Thread.sleep(PERIOD);

        }
        log.info("Sender ended job");

    }
}
